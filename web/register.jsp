<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<%@taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <link rel="stylesheet" type="text/css" href="css/Mon_style.css"/>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>index</title>
</head>
<body>
<h1>Your informations</h1>
<p>Email: <s:property value="user.email"/></p>
<p>Password: <s:property value="user.password"/></p>
<p>Gender: <s:property value="user.gender"/></p>
</body>
</html>