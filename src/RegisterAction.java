import com.opensymphony.xwork2.ActionSupport;

public class RegisterAction extends ActionSupport {

    private User user;

    @Override
    public String execute() throws Exception {
        return SUCCESS;
    }


    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
